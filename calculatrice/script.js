const form = document.querySelector('#form');

form.addEventListener('submit', (event) => {
    event.preventDefault(); // Empêche le rechargement de la page

    const num1 = parseFloat(form.querySelector('#num1').value);
    const num2 = parseFloat(form.querySelector('#num2').value);
    const operator = form.querySelector('#operator').value;

    if (isNaN(num1) || isNaN(num2)) {
        alert('Please enter valid numbers');
    } else {
        let res;
        switch(operator) {
            case "Add":
                res = num1 + num2;
                break;
            case "Subtract":
                res = num1 - num2;
                break;
            case "Multiply":
                res = num1 * num2;
                break;
            case "Divide":
                if (num2 !== 0) {
                    res = num1 / num2;
                } else {
                    alert("Cannot divide by zero");
                    return;
                }
                break;
            default:
                alert("Invalid Operator");
                return;
        };
        alert("The result is : " + res);
    }
});
