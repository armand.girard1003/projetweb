const express = require('express');
const fs = require('fs');

const app = express();

const port = 3000;
const host = '10.49.27.33';

const homePath = './home.html';

app.get('/', (req, res) => {
    fs.readFile(homePath, 'utf-8', (err, data) => { // Correction ici

        if (err) {
            console.error('Error reading file: ', err);
            res.status(500).send('Sorry, out of order');
        }
        else {
            res.send(data);
        }
    });
});

app.listen(port, host, () => { 
    console.log('App available on http://' + host + ':' + port);
});
