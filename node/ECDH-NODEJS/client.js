// Import required modules
const net = require('net');
const crypto = require('crypto');

// Define constants for the server host and port
const HOST = '10.49.27.4';
const PORT = 8283;

// Create an Elliptic Curve Diffie-Hellman (ECDH) key pair for the client
const ecdh = crypto.createECDH('secp256k1');
ecdh.generateKeys();

// Initialize variables to store the server's public key and the shared secret key
let serverPublicKey;
let sharedSecret = null;

// Define a message to be sent to the server
const messageDecrypted = "Hello from client!";

/**
 * Encrypt a message using AES-256-CBC
 * @param {string} message - The message to be encrypted
 * @param {string} key - The secret key to use for encryption
 * @returns {object} - An object containing the encrypted message and the initialization vector
 */
function encryptMessage(message, key) {
  // Generate a random initialization vector (IV)
  const iv = crypto.randomBytes(16);
  // Create an AES-256-CBC cipher with the given key and IV
  const cipher = crypto.createCipheriv('aes-256-cbc', Buffer.from(key, 'hex'), iv);
  // Encrypt the message
  let encryptedMessage = cipher.update(message, 'utf8', 'base64');
  encryptedMessage += cipher.final('base64');
  return { iv: iv.toString('base64'), encryptedMessage };
}

/**
 * Decrypt a message using AES-256-CBC
 * @param {object} encryptedData - An object containing the encrypted message and the initialization vector
 * @param {string} key - The secret key to use for decryption
 * @returns {string} - The decrypted message
 */
function decryptMessage(encryptedData, key) {
  // Create an AES-256-CBC decipher with the given key and IV
  const decipher = crypto.createDecipheriv('aes-256-cbc', Buffer.from(key, 'hex'), Buffer.from(encryptedData.iv, 'base64'));
  // Decrypt the message
  let decryptedMessage = decipher.update(encryptedData.encryptedMessage, 'base64', 'utf8');
  decryptedMessage += decipher.final('utf8');
  return decryptedMessage;
}

// Create a TCP client
const client = net.createConnection(PORT, HOST, () => {
  // Send the client's public key to the server
  client.write(ecdh.getPublicKey());

  // Handle incoming data from the server
  client.on('data', (data) => {
    // Store the server's public key
    serverPublicKey = data;
    if (sharedSecret == null && serverPublicKey!= null) {
      // Compute the shared secret key using ECDH
      sharedSecret = ecdh.computeSecret(serverPublicKey, 'base64', 'hex');
      console.log('\n\nShared secret:', sharedSecret);
    } else if (JSON.parse(data).message!= null) {
      // Encrypt the message using the shared secret key
      const encryptedMessage = encryptMessage(messageDecrypted, sharedSecret);
      // Send the encrypted message to the server
      client.write(JSON.stringify({ message: encryptedMessage }));

      // Decrypt the incoming message using the shared secret key
      const message = JSON.parse(data).message;
      console.log('Encrypted message:', message);
      const decryptedMessage = decryptMessage(message, sharedSecret);
      console.log('Decrypted message:', decryptedMessage + '\n\n');
      client.end();
    }
  });
});