// Import required modules
const crypto = require('crypto');
const net = require('net');

// Define the port number
const port = 8283;

// Create an Elliptic Curve Diffie-Hellman (ECDH) key pair for ecdh
const ecdh = crypto.createECDH('secp256k1');
ecdh.generateKeys();

// Initialize variables to store the client's public key and the shared secret key
let publicClientKey;
let sharedKey = null;

// Define a message to be sent to the client
const message = "Hello, this is a message from the server";

/**
 * Encrypt a message using AES-256-CBC
 * @param {string} message - The message to be encrypted
 * @param {string} key - The secret key to use for encryption
 * @returns {object} - An object containing the encrypted message and the initialization vector
 */
function encryptMessage(message, key) {
  // Generate a random initialization vector (IV)
  const iv = crypto.randomBytes(16);
  // Create an AES-256-CBC cipher with the given key and IV
  const cipher = crypto.createCipheriv('aes-256-cbc', Buffer.from(key, 'hex'), iv);
  // Encrypt the message
  let encryptedMessage = cipher.update(message, 'utf8', 'base64');
  encryptedMessage += cipher.final('base64');
  return { iv: iv.toString('base64'), encryptedMessage };
}

/**
 * Decrypt a message using AES-256-CBC
 * @param {object} encryptedData - An object containing the encrypted message and the initialization vector
 * @param {string} key - The secret key to use for decryption
 * @returns {string} - The decrypted message
 */
function decryptMessage(encryptedData, key) {
  // Create an AES-256-CBC decipher with the given key and IV
  const decipher = crypto.createDecipheriv('aes-256-cbc', Buffer.from(key, 'hex'), Buffer.from(encryptedData.iv, 'base64'));
  // Decrypt the message
  let decryptedMessage = decipher.update(encryptedData.encryptedMessage, 'base64', 'utf8');
  decryptedMessage += decipher.final('utf8');
  return decryptedMessage;
}

// Create a TCP server
const network = net.createServer((socket) => {
  // Send ecdh's public key to the client
  socket.write(ecdh.getPublicKey());

  // Handle incoming data from the client
  socket.on('data', (data) => {
    // Store the client's public key
    publicClientKey = data;
    if (sharedKey == null) {
      // Compute the shared secret key using ECDH
      sharedKey = ecdh.computeSecret(publicClientKey, 'base64', 'hex');
      console.log('\n\nShared key: ' + sharedKey);

      // Encrypt the message using the shared secret key
      const encryptedData = encryptMessage(message, sharedKey);
      // Send the encrypted message to the client
      socket.write(JSON.stringify({ message: encryptedData }));
    } else if (JSON.parse(data).message!= null) {
      // Decrypt the incoming message using the shared secret key
      const message = JSON.parse(data).message;
      console.log('Encrypted message: ', message);
      const decryptedMessage = decryptMessage(message, sharedKey);
      console.log('Decrypted message: ' + decryptedMessage + '\n\n');
      network.close();
    }
  });
}).listen(port, () => {});